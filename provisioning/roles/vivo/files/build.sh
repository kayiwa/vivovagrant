#!/bin/bash
set -x
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64
export CATALINA_HOME=/usr/share/tomcat7
export VITRO_HOME_DIR=/var/local/vadm/data
export TOMCAT_DIR=/usr/share/tomcat7


cd /var/local/vadm/vivo/vivo-rel-1.7
cp example.build.properties build.properties

## 38c38
## want default this time but next time ...
sed -i -e "/webapp.name/s:vivo:vivo:" build.properties
## 44c44
sed -i -e "/^vitro.home = /s:/usr/local/vivo/home:$VITRO_HOME_DIR:" build.properties
## 44c44
sed -i -e "/^tomcat.home = /s:/usr/local/tomcat:$TOMCAT_DIR:" build.properties


ant all > ant_all.log 2>&1


cd {{ vivo_data_dir }}
cp example.runtime.properties runtime.properties
# diff example.runtime.properties runtime.properties
# 28c28
sed -i -e '/^Vitro.defaultNamespace/s:vivo.mydomain.edu:{{ vitro_default_namespace }}:' runtime.properties

# 35c35
sed -i -e '/^rootUser.emailAddress/s:vivo_root@mydomain.edu:{{ vivo_root_email_address }}:' runtime.properties

# 42c42
sed -i -e '/^VitroConnection.DataSource.url/s/vitrodb/vivo/' runtime.properties

# 43c43
sed -i -e '/^VitroConnection.DataSource.username/s/vitrodbUsername/vivo/' runtime.properties

# 44c44
sed -i -e '/^VitroConnection.DataSource.password/s/vitrodbPassword/vivo/' runtime.properties

# 51c51
sed -i -e '/^email.smtpHost/s/smtp.mydomain.edu/smtp.vt.edu/' runtime.properties

# 51c51
sed -i -e '/^email.replyTo/s/vivoAdmin@mydomain.edu/{{ vivo_admn_email_address }}/' runtime.properties

# 75c75
sed -i -e '/^selfEditing.idMatchingProperty/s/vivo.mydomain.edu/localhost/vivo/' runtime.properties
