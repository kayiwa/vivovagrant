# README #

You will need to have the latest [Vagrant](http://vagrantup.com), [Ansible](http://ansible.com) installed on your machine

### What is this repository for? ###

* This repository will install [VIVO](http://vivoweb.org)

### How do I get set up? ###

* clone this repo
* cd into the directory `cd <repo name>/provisioning`
* run `cp example_secret_things.yml secret_things.yml`
* modify those as needed
* then run `vagrant up`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
